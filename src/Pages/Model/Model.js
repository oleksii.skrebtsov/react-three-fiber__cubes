import React, { Suspense } from "react";
import { Canvas } from "@react-three/fiber";

import { Skull } from "../../Components/Skull";
import { Textures } from "../../Components/Textures";
import { Rig } from "../../Components/Rig";
import { OrbitControls } from "@react-three/drei";

import "./Model.scss";
import { Link } from "react-router-dom";

const SkullName = () => {
  return (
    <div className='skull-name'>
      <h1>Hello, I am cool skull !</h1>
    </div>
  );
};

const HomeLink = () => {
  return (
    <div className='home-link'>
      <Link to='/'>Home</Link>
    </div>
  );
};

export const Model = () => {
  return (
    <>
      <Suspense fallback={<h1>Loading...</h1>}>
        <Canvas camera={{ position: [0, 10, -30] }}>
          <Skull />
          <Textures />
          <Rig />
          <OrbitControls enableZoom={false} />
          <directionalLight position={[5, 35, -20]} intensity={2} />
        </Canvas>
        <SkullName />
        <HomeLink />
      </Suspense>
    </>
  );
};
