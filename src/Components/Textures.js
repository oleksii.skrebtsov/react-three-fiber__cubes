import { useThree } from "@react-three/fiber";
import { CubeTextureLoader } from "three";

import texture1 from "../Assets/brick.jpg";
import texture2 from "../Assets/sky.jpg";
import texture3 from "../Assets/tile.jpg";

export const Textures = () => {
  const { scene } = useThree();
  const loader = new CubeTextureLoader();
  const texture = loader.load([texture1, texture1, texture2, texture3, texture1, texture1]);
  scene.background = texture;

  return null;
};
