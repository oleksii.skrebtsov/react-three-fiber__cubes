import React from "react";
import { Link } from "react-router-dom";
import { Physics } from "@react-three/cannon";
import { Canvas } from "@react-three/fiber";
import { OrbitControls, softShadows } from "@react-three/drei";
import "./Home.scss";

import { Plane } from "../../Components/Plane";
import { Cubes } from "../../Components/Cubes";
import { Sphere } from "../../Components/Sphere";

softShadows();

export const Home = () => {
  return (
    <>
      <h1>First 3D App with ThreeJS</h1>
      <Link to='3D-object'>Link to 3D-object</Link>

      <Canvas shadows colorManagement camera={{ position: [-5, 2, 10], fov: 60 }}>
        <ambientLight intensity={0.3} />
        <directionalLight
          castShadow
          position={[0, 10, 0]}
          intensity={1.5}
          shadow-mapSize-width={1024}
          shadow-mapSize-height={1024}
          shadow-camera-far={50}
          shadow-camera-left={-10}
          shadow-camera-right={20}
          shadow-camera-top={20}
          shadow-camera-bottom={-10}
        />
        <pointLight position={[-10, 0, -20]} intensity={0.5} />
        <pointLight position={[0, -10, 0]} intensity={1.5} />

        <Physics gravity={[0, -50, 0]} defaultContactMaterial={{ restitution: 0.7 }} >
          <Cubes position={[0, 1, 0]} args={[3, 2, 1]} color='lightblue' speed={2} factor={0.1} />
          <Cubes position={[-2, 1, -5]} args={[1, 2, 1]} color='pink' speed={4} factor={0.2} />
          <Cubes position={[8, 1, -2]} args={[1, 1, 1]} color='yellow' speed={4} factor={0.5} />
          <Sphere />
          <Plane />
        </Physics>
        
        <OrbitControls 
        // autoRotate={true} 
        />
      </Canvas>
    </>
  );
};
