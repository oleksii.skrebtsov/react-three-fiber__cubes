import React from "react";
import { Switch, Route } from "react-router-dom";
import { Home } from "./Home/Home";
import { Model } from "./Model/Model";

export const Routing = () => (
  <Switch>
    <Route exact path='/' component={Home} />
    <Route exact path='/3D-object' component={Model} />
  </Switch>
);
