import React, { useRef, useState } from "react";
import { useFrame } from "@react-three/fiber";
import { MeshWobbleMaterial } from "@react-three/drei";
import { useSpring, animated } from "@react-spring/three";

export const Cubes = ({ position, args, color, factor, speed }) => {
  const mesh = useRef(null);
  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.01));

  const [active, setActive] = useState(false);

  const theScale = useSpring({
    scale: active ? [1.2, 1.2, 1.2] : [1, 1, 1],
  });

  return (
    <animated.mesh
      onClick={() => setActive(!active)}
      scale={theScale.scale}
      castShadow
      position={position}
      ref={mesh}
    >
      <boxBufferGeometry attach='geometry' args={args} />
      <MeshWobbleMaterial attach='material' color={color} speed={speed} factor={factor} />
    </animated.mesh>
  );
};
