import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import { Routing } from "./Pages/Routing";

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Routing />
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
