import React, { useEffect, useState } from "react";
import { useLoader } from "@react-three/fiber";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader";
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader";
import { useSpring, animated } from "@react-spring/three";

export const Skull = () => {
    const [active, setActive] = useState(false);
  
    useEffect(() => {
      document.body.style.cursor = active ? "pointer" : "auto";
    }, [active]);
  
    const material = useLoader(MTLLoader, "./12140_Skull_v3_L2.mtl");
    const obj = useLoader(OBJLoader, "./12140_Skull_v3_L2.obj", (loader) => {
      material.preload();
      loader.setMaterials(material);
    });
  
    const theScale = useSpring({
      scale: active ? 0.5 : 0.45,
    });
  
    return (
      <>
        <animated.primitive
          object={obj}
          scale={theScale.scale}
          rotation={[-Math.PI / 2, 0, Math.PI / 1]}
          onPointerEnter={() => setActive(true)}
          onPointerLeave={() => setActive(false)}
        />
      </>
    );
  };