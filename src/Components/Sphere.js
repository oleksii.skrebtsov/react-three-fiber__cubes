import { useSphere } from "@react-three/cannon";

export const Sphere = () => {
  const [ref] = useSphere(() => ({
    mass: 5,
    position: [10, 9, -10],
  }));
  return (
    <>
      <mesh castShadow ref={ref}>
        <sphereBufferGeometry />
        <meshStandardMaterial color='red' />
      </mesh>
    </>
  );
};
