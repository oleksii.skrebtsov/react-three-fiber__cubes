import * as THREE from "three";
import { useFrame, useThree } from "@react-three/fiber";

export const Rig = () => {
  const { camera, mouse } = useThree();
  const vector = new THREE.Vector3();
  return useFrame(() => camera.position.lerp(vector.set(mouse.x * 5, -mouse.y + 5, -25), 0.1));
};
